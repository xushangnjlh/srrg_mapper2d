# srrg_mapper2d

This package contains the basic functions and utilities for implementing a 2D graph-based SLAM system.

## Prerequisites

This package requires:
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_scan_matcher](https://gitlab.com/srrg-software/srrg_scan_matcher)

This package depends on **g2o**:
* [g2o](https://github.com/RainerKuemmerle/g2o)

## Installation

Clone and compile the above mentioned packages on your catkin workspace.
For a clean installation of the `srrg` environment check [srrg_scripts](https://gitlab.com/srrg-software/srrg_scripts) using as option `packages_mapper2d.txt` during the setup.

## Usage

This package does not provide a full application itself. In order to try our SLAM system check the following packages and follow the instructions therein:  
* [srrg_mapper2d_gui](https://gitlab.com/srrg-software/srrg_mapper2d_gui)
* [srrg_mapper2d_ros](https://gitlab.com/srrg-software/srrg_mapper2d_ros)
