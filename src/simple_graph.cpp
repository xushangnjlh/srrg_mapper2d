#include "simple_graph.h"
#include "projective_validator2d.h"

namespace srrg_mapper2d {

  /////////////////
  //Helper function
  void addToPrunedMap(std::map<DiscreteTriplet, EdgeSE2*>& myMap, EdgeSE2* candidate, float dx, float dy, float dth){
    Eigen::Vector3f result = t2v(candidate->measurement().toIsometry().cast<float>());
    DiscreteTriplet currentTriplet(result, dx, dy, dth);
    std::map<DiscreteTriplet, EdgeSE2*>::iterator it=myMap.find(currentTriplet);
    if (it==myMap.end())
      myMap.insert(std::make_pair(currentTriplet, candidate));
  };
  /////////////////

  SimpleGraph::SimpleGraph(){
    _graph = new SparseOptimizer();

    //Init graph
    SlamLinearSolver* linearSolver = new SlamLinearSolver();
    linearSolver->setBlockOrdering(false);
    SlamBlockSolver* blockSolver = new SlamBlockSolver(std::unique_ptr<SlamLinearSolver>(linearSolver));
    //OptimizationAlgorithmGaussNewton* solver = new OptimizationAlgorithmGaussNewton(blockSolver);
    OptimizationAlgorithmLevenberg* solver = new OptimizationAlgorithmLevenberg(std::unique_ptr<SlamBlockSolver>(blockSolver));
    _graph->setAlgorithm(solver);
    _graph->setVerbose(false);

    //create projector of 2*M_PI
    float fov = 2*M_PI;
    int num_ranges = 720;
    _projector = new Projector2D;
    _projector->setFov(fov);
    _projector->setNumRanges(num_ranges);

    //Init Cloud Aligner
    _c2DAligner=new Cloud2DAligner(_projector);
    _c2DAligner->useNNCorrespondenceFinder();

    _currentVertex = 0;
    _currentId = 0;
    _currentSessionId = _currentId;
    _isFirstVertex = true;

    _loopClosureWindow = 10;
    _loopClosureInlierThreshold = 3.0;
    _loopClosureMinInliers = 6;

    float _inliersDistance = 0.05;
    float _minInliersRatio = 0.5;
    int _minNumCorrespondences = 50;

    _c2DAligner->setInliersDistance(_inliersDistance);
    _c2DAligner->setMinInliersRatio(_minInliersRatio);
    _c2DAligner->setMinNumCorrespondences(_minNumCorrespondences);

    _num_ranges = 0;
    _fov = 0.0;

    delta_closure_translation_cropping_threshold=1.0f;

  }

  void SimpleGraph::addVertex(Eigen::Vector3f& pose, Cloud2DWithTrajectory* cloud){

    SE2 estimate(pose.x(), pose.y(), pose.z());
    
    VertexSE2* v = new VertexSE2;
    v->setEstimate(estimate);
    v->setId(_currentId++);

    cloud->setColor(Eigen::Vector3f(0,0.4,0));
    _verticesClouds.insert(std::make_pair(v,cloud) );

    _currentVertex = v;

    if (_isFirstVertex){
      _currentVertex->setFixed(true); //Fix the first vertex
      _isFirstVertex = false;
    }
    _graph->addVertex(currentVertex());

    for (VertexSE2Cloud2DMap::iterator it = _verticesClouds.begin(); it != _verticesClouds.end(); it++){
      Cloud2DWithTrajectory* c = it->second;
      VertexSE2* v = it->first;
      if (v->id() >= _currentSessionId) //only set color on clouds from the new session
	c->setColor(Eigen::Vector3f(0,0.4,0));
    }
  }

  EdgeSE2* SimpleGraph::createEdge(VertexSE2* vfrom, VertexSE2* vto, SE2& relative_transf, Eigen::Matrix3d& inf){

    EdgeSE2 *e = new EdgeSE2;
    e->vertices()[0] = vfrom;
    e->vertices()[1] = vto;
    e->setMeasurement(relative_transf);
    e->setInformation(inf);

    return e;
  }

  void SimpleGraph::addEdge(VertexSE2* vfrom, VertexSE2* vto, SE2& relative_transf, Eigen::Matrix3d& inf){

    EdgeSE2 *e = createEdge(vfrom, vto, relative_transf, inf);

    _graph->addEdge(e);
  }

  void SimpleGraph::matchVertex(VertexSE2* v){
    cerr << "\nFinding constraints... ";

    //graph is optimized first so last added edge is satisfied
    optimize(5);

    VerticesFinder vf(_graph);
    OptimizableGraph::VertexSet vset;
    std::set<OptimizableGraph::VertexSet> setOfVSet;
    vf.findVerticesScanMatching(v, vset);
    if (vset.size()){
      vf.checkCovariance(vset, v);
      vf.findSetsOfVertices(vset, setOfVSet);
    }

    //Update ellipse data for current vertex
    VertexEllipse* ellipse = findEllipseData(v);
    if (ellipse){
      for (OptimizableGraph::VertexSet::iterator itv = vset.begin(); itv != vset.end(); itv++){
	VertexSE2 *vertex = (VertexSE2*) *itv;
	SE2 relativetransf = v->estimate().inverse() * vertex->estimate();
	ellipse->addMatchingVertex(relativetransf.translation().x(), relativetransf.translation().y());
	ellipse->addMatchingVertexID(vertex->id());
      }
    }
    if (!vset.size())
      cerr << "No vertices to match" << endl;
    else
      cerr << vset.size() <<  " candidate vertices to match" << endl;



    OptimizableGraph::EdgeSet loopClosingEdges;
    for (std::set<OptimizableGraph::VertexSet>::iterator it = setOfVSet.begin(); it != setOfVSet.end(); it++) {
      OptimizableGraph::VertexSet myvset = *it;
      VertexSE2* otherv = dynamic_cast<VertexSE2*> (vf.findClosestVertex(myvset, v));

      //Trying relative transformation as initial guess
      Eigen::Isometry2d delta=v->estimate().toIsometry().inverse()*otherv->estimate().toIsometry();
      bool success = tryAlign(v,otherv,delta.cast<float>());

      if (success) {
	cerr << "Success. ";
	if (abs(otherv->id() - v->id()) < 2){
	  //Consecutive vertices
	  cerr << "Adding edge." << endl;
	  //consecutive vertices
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  //directly add
	  addEdge(v, otherv, relative_transf, inf);
	} else {
	  cerr << "Adding edge as a closure candidate." << endl;
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  EdgeSE2 *candidate_edge = createEdge(v, otherv, relative_transf, inf);
	  loopClosingEdges.insert(candidate_edge);
	}
	continue;
      }

      //Trying zero transformation as initial guess
      success = tryAlign(v,otherv);
      if (success) {
	cerr << "Success. ";
	if (abs(otherv->id() - v->id()) < 2){
	  //Consecutive vertices
	  cerr << "Adding edge." << endl;
	  //consecutive vertices
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  //directly add
	  addEdge(v, otherv, relative_transf, inf);
	} else {
	  cerr << "Adding edge as a closure candidate." << endl;
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  EdgeSE2 *candidate_edge = createEdge(v, otherv, relative_transf, inf);
	  loopClosingEdges.insert(candidate_edge);
	}
	continue;
      }

      //Trying switch ref-cur with relative transformation as initial guess
      delta=otherv->estimate().toIsometry().inverse()*v->estimate().toIsometry();
      success = tryAlign(otherv,v,delta.cast<float>());
      if (success) {
	cerr << "Success. ";
	if (abs(otherv->id() - v->id()) < 2){
	  //Consecutive vertices
	  cerr << "Adding edge." << endl;
	  //consecutive vertices
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  //directly add
	  addEdge(otherv, v, relative_transf, inf);
	} else {
	  cerr << "Adding edge as a closure candidate." << endl;
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  EdgeSE2 *candidate_edge = createEdge(otherv, v, relative_transf, inf);
	  loopClosingEdges.insert(candidate_edge);
	}
	continue;
      }
    
      //Trying zero transformation as initial guess
      success = tryAlign(otherv,v);
      if (success) {
	cerr << "Success. ";
	if (abs(otherv->id() - v->id()) < 2){
	  //Consecutive vertices
	  cerr << "Adding edge." << endl;
	  //consecutive vertices
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  //directly add
	  addEdge(otherv, v, relative_transf, inf);
	} else {
	  cerr << "Adding edge as a closure candidate." << endl;
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  EdgeSE2 *candidate_edge = createEdge(otherv, v, relative_transf, inf);
	  loopClosingEdges.insert(candidate_edge);
	}
	continue;
      }
    
      //we also try the pose of other vertices in covariance threshold as initial guess
      OptimizableGraph::VertexSet vsetInCov;
      vf.findVerticesInCovariance(v, otherv, vsetInCov);

      for (OptimizableGraph::VertexSet::iterator itcov = vsetInCov.begin(); itcov != vsetInCov.end(); itcov++) {
	VertexSE2* vertexcov = dynamic_cast<VertexSE2*> (*itcov);
	if (vertexcov->id() == otherv->id())
	  continue;

	cerr << "Vertex " << vertexcov->id() << " is in covariance with " << otherv->id() << endl;

	delta = vertexcov->estimate().toIsometry().inverse()*otherv->estimate().toIsometry();

	cerr << "Tryalign vertex in covariance." << endl;
	success = tryAlign(v, otherv, delta.cast<float>());
	if (success){
	  cerr << "Success. Adding edge from vertex in covariance as a closure candidate." << endl;
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  EdgeSE2 *candidate_edge = createEdge(v, otherv, relative_transf, inf);
	  loopClosingEdges.insert(candidate_edge);
	  break;
	}
      }
    }

    cerr << "Number of found closures: " << loopClosingEdges.size() << endl;
    if (loopClosingEdges.size()){
      _candidate_closures.addEdgeSet(loopClosingEdges);
      _candidate_closures.addVertex(v);
    }
    checkClosures();
  
  }









  
  /*
  void SimpleGraph::matchVertex(VertexSE2* v){
    cerr << "\nFinding constraints... ";
    Cloud2DWithTrajectory* c = vertexCloud(v);
    _c2DAligner->setReference(c);
    std::cerr << "Reference cloud initial pose: " << t2v(c->pose()).transpose() << std::endl;

    //graph is optimized first so last added edge is satisfied
    optimize(5);

    VerticesFinder vf(_graph);
    OptimizableGraph::VertexSet vset;
    std::set<OptimizableGraph::VertexSet> setOfVSet;
    vf.findVerticesScanMatching(v, vset);
    if (vset.size()){
      vf.checkCovariance(vset, v);
      vf.findSetsOfVertices(vset, setOfVSet);
    }

    //Update ellipse data for current vertex
    VertexEllipse* ellipse = findEllipseData(v);
    if (ellipse){
      for (OptimizableGraph::VertexSet::iterator itv = vset.begin(); itv != vset.end(); itv++){
	VertexSE2 *vertex = (VertexSE2*) *itv;
	SE2 relativetransf = v->estimate().inverse() * vertex->estimate();
	ellipse->addMatchingVertex(relativetransf.translation().x(), relativetransf.translation().y());
	ellipse->addMatchingVertexID(vertex->id());
      }
    }
    if (!vset.size())
      cerr << "No vertices to match" << endl;
    else
      cerr << vset.size() <<  " candidate vertices to match" << endl;

    OptimizableGraph::EdgeSet loopClosingEdges;
    for (std::set<OptimizableGraph::VertexSet>::iterator it = setOfVSet.begin(); it != setOfVSet.end(); it++) {
      OptimizableGraph::VertexSet myvset = *it;
      VertexSE2* otherv = dynamic_cast<VertexSE2*> (vf.findClosestVertex(myvset, v));
      //for (OptimizableGraph::VertexSet::iterator it = vset.begin(); it != vset.end(); it++) {
      //VertexSE2* otherv = dynamic_cast<VertexSE2*> (*it);

      cerr << ">>>>>>>>>>Trying match with vertex " << otherv->id() << endl;
      Cloud2DWithTrajectory* otherc= vertexCloud(otherv);
      _c2DAligner->setCurrent(otherc);
      std::cerr << "Current cloud initial pose: " << t2v(otherc->pose()).transpose() << std::endl;

      bool success = false;
      Eigen::Isometry2f delta=c->pose().inverse()*otherc->pose();
      if (abs(otherv->id() - v->id()) < 2){ // 10){
	//close vertex, we try their relative transf as initial guess
	success = tryAlign(delta);
	if (success){
	  cerr << "Success. ";
	  cerr << "Adding edge." << endl;
	  //consecutive vertices
	  SE2 relative_transf = getSE2TransfFromAligner();
	  Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
	  //directly add
	  addEdge(v, otherv, relative_transf, inf);
	}

	//logging closure
	_c2DAligner->logAlign(v->id(), otherv->id());
      }else{
	std::map<DiscreteTriplet, EdgeSE2*> candidateEdges;

	//not so close vertex, we try their relative transf as initial guess
	success = tryAlign(delta);
	if (success){
	  cerr << "Success. Adding edge as a closure candidate." << endl;
	  createCandidateEdgeFromAligner(v, otherv, candidateEdges);
	}

	//logging closure
	_c2DAligner->logAlign(v->id(), otherv->id());

	//we try their relative transf cropped as initial guess
	Eigen::Isometry2f cropped_delta=delta;
	float delta_norm=cropped_delta.translation().norm();
	if (delta_norm>delta_closure_translation_cropping_threshold){
	  cropped_delta.translation()*=delta_closure_translation_cropping_threshold/delta_norm;
	  cerr << "Tryalign cropped stuff." << endl;
	  success = tryAlign(cropped_delta);
	  if (success){
	    cerr << "Success. Adding edge as a closure candidate with cropped delta." << endl;
	    createCandidateEdgeFromAligner(v, otherv, candidateEdges);
	  }

	  //logging closure
	  _c2DAligner->logAlign(v->id(), otherv->id());
	}

	//we also try the pose of the vertex as initial guess
	delta.translation().setZero();
	success = tryAlign(delta);
	if (success){
	  cerr << "Success. Adding edge as a closure candidate." << endl;
	  createCandidateEdgeFromAligner(v, otherv, candidateEdges);
	}

	//logging closure
	_c2DAligner->logAlign(v->id(), otherv->id());

	////we also try with the waypoints as initial guess
	//Cloud2DWithTrajectory* cloudTrajectory = dynamic_cast<Cloud2DWithTrajectory*>(otherc);
	//  if (cloudTrajectory){
	//    for (size_t wp = 0; wp < cloudTrajectory->trajectory().size(); wp++){
	//      delta.translation() = cloudTrajectory->waypoint(wp).translation();
	//      cerr << "Tryalign cloudtrajectory." << endl;
	//      success = tryAlign(delta);
	//      if (success){
        //        cerr << "Success. Adding edge from wp as a closure candidate." << endl;
        //        createCandidateEdgeFromAligner(v, otherv, candidateEdges);
	//      }
	//      //logging closure
	//      _c2DAligner->logAlign(v->id(), otherv->id());
	//   }
	// }

	//we also try the pose of other vertices in covariance threshold as initial guess
	OptimizableGraph::VertexSet vsetInCov;
	vf.findVerticesInCovariance(v, otherv, vsetInCov);

	for (OptimizableGraph::VertexSet::iterator itcov = vsetInCov.begin(); itcov != vsetInCov.end(); itcov++) {
	  VertexSE2* vertexcov = dynamic_cast<VertexSE2*> (*itcov);
	  if (vertexcov->id() == otherv->id())
	    continue;

	  cerr << "Vertex " << vertexcov->id() << " is in covariance with " << otherv->id() << endl;

	  Eigen::Isometry2d transfvcov = vertexcov->estimate().toIsometry().inverse()*otherv->estimate().toIsometry();
	  delta= transfvcov.cast<float>();

	  cerr << "Tryalign vertex in covariance." << endl;
	  success = tryAlign(delta);
	  if (success){
	    cerr << "Success. Adding edge from vertex in covariance as a closure candidate." << endl;
	    createCandidateEdgeFromAligner(v, otherv, candidateEdges);
	  }

	  //logging closure
	  _c2DAligner->logAlign(v->id(), otherv->id());
	}

	for (std::map<DiscreteTriplet, EdgeSE2*>::iterator it=candidateEdges.begin(); it!= candidateEdges.end(); it++)
	  loopClosingEdges.insert(it->second);
      }
    }

    cerr << "Number of pruned closures: " << loopClosingEdges.size() << endl;
    if (loopClosingEdges.size()){
      _candidate_closures.addEdgeSet(loopClosingEdges);
      _candidate_closures.addVertex(v);
    }
    checkClosures();

  }*/


  bool SimpleGraph::tryAlign(VertexSE2* v_ref, VertexSE2* v_cur, const Eigen::Isometry2f& initial_guess) {

    std::cerr << ">>>>>>>>>>Trying match: " << v_ref->id() << " " << v_cur->id() << std::endl;

    std::cerr << "Reference cloud initial pose: " << t2v(vertexCloud(v_ref)->pose()).transpose() << std::endl;
    std::cerr << "Current cloud initial pose: " << t2v(vertexCloud(v_cur)->pose()).transpose() << std::endl;

    //Init aligner
    _c2DAligner->setReference(vertexCloud(v_ref));
    _c2DAligner->setCurrent(vertexCloud(v_cur));

    cerr << "Trying align" << endl;
    _c2DAligner->compute(initial_guess);
    return _c2DAligner->validate();
  }

  bool SimpleGraph::tryAlign(Eigen::Isometry2f& initial_guess){
    cerr << "Trying align" << endl;
    _c2DAligner->compute(initial_guess);
    return _c2DAligner->validate();
  }

  SE2 SimpleGraph::getSE2TransfFromAligner(){
    SE2 transf(_c2DAligner->T().cast<double>());
    return transf;
  }

  void SimpleGraph::createCandidateEdgeFromAligner(VertexSE2* vfrom, VertexSE2* vto, std::map<DiscreteTriplet, EdgeSE2*>& candidateEdges){

    SE2 relative_transf = getSE2TransfFromAligner();
    Eigen::Matrix3d inf = _c2DAligner->alignInformationMatrix().cast<double>();
    EdgeSE2 *candidate_edge = createEdge(vfrom, vto, relative_transf, inf);

    float dx = 0.2, dy = 0.2, dth = 0.3; //Results discretization
    addToPrunedMap(candidateEdges, candidate_edge, dx, dy, dth);
  }

  void SimpleGraph::optimize(int nrunnings){
    if (graph()->vertices().size()>1){
      graph()->initializeOptimization();
      graph()->optimize(nrunnings);

      //update clouds pose
      for (VertexSE2Cloud2DMap::iterator it = _verticesClouds.begin(); it != _verticesClouds.end(); it++){
	VertexSE2* v = dynamic_cast<VertexSE2*>(it->first);
	Cloud2DWithTrajectory* c = it->second;

	Eigen::Isometry2f pose = v->estimate().toIsometry().cast<float>();
	c->setPose(pose);
      }
    }
  }

  Cloud2DWithTrajectory* SimpleGraph::vertexCloud(VertexSE2* v){
    VertexSE2Cloud2DMap::iterator it = _verticesClouds.find(v);
    if (it!=_verticesClouds.end())
      return it->second;
    else
      return 0;
  }

  bool SimpleGraph::saveGraph(const char *filename){
    //Transform clouds to laser scans
    for (VertexSE2Cloud2DMap::iterator it = _verticesClouds.begin(); it != _verticesClouds.end(); it++){
      VertexSE2* v = dynamic_cast<VertexSE2*>(it->first);
      addScanFromCloud(v);
    }
    return _graph->save(filename);
  }

  void SimpleGraph::saveClouds(const char* prefix){

    //Transform clouds to laser scans
    for (VertexSE2Cloud2DMap::iterator it = _verticesClouds.begin(); it != _verticesClouds.end(); it++){
      VertexSE2* v = dynamic_cast<VertexSE2*>(it->first);

      char cloudfilename[1024];
      if (prefix)
	sprintf(cloudfilename, "%s-cloud-%05d.dat", prefix, v->id());
      else
	sprintf(cloudfilename, "cloud-%05d.dat", v->id());
      ofstream cloud_stream(cloudfilename);
      Cloud2DWithTrajectory* c = vertexCloud(v);
      c->save(cloud_stream);
    }
  }

  void SimpleGraph::addScanFromCloud(VertexSE2* v){
    RobotLaser* rlaser = findLaserData(v);
    if (rlaser){
      //this vertex already has a laser data but we need to update its odomPose
      rlaser->setOdomPose(v->estimate());
      return;
    }

    rlaser = computeScanFromCloud(v);
    v->addUserData(rlaser);
  }

  RobotLaser* SimpleGraph::computeScanFromCloud(VertexSE2* v, const Eigen::Isometry2f& transform){

    Cloud2DWithTrajectory* c = _verticesClouds[v];

    //project current cloud to ranges
    float fov = M_PI;
    int num_ranges = 720;

    Projector2D projector;
    projector.setFov(fov);
    projector.setNumRanges(num_ranges);

    FloatVector ranges;
    IntVector indices;
    projector.project(ranges, indices, transform, *c);

    //create g2o RobotLaser data from ranges
    LaserParameters lparams(0, projector.numRanges(), -M_PI/2, projector.angleIncrement(), projector.maxRange(), 0.1, 0);
    SE2 trobotlaser(0, 0, 0); //TODO
    lparams.laserPose = trobotlaser;


    RobotLaser* rlaser = new RobotLaser;
    rlaser->setLaserParams(lparams);
    Eigen::Isometry2f estimate = v->estimate().toIsometry().cast<float>()*transform;
    rlaser->setOdomPose(estimate.cast<double>());
    rlaser->setHostname("hostname");
    std::vector<double> rangesd(ranges.begin(), ranges.end());
    rlaser->setRanges(rangesd);

    return rlaser;
  }

  RobotLaser* SimpleGraph::computeScanFromCloudWaypoint(VertexSE2* v, const Eigen::Isometry2f& transform, size_t wp){

    Cloud2DWithTrajectory* c = (Cloud2DWithTrajectory*) _verticesClouds[v];
    srrg_core::LaserMessage* originallaser = c->laserWaypoint(wp);

    //project current cloud to ranges


    Projector2D projector;
    int num_ranges_projector;
    if (_num_ranges){
      num_ranges_projector = _num_ranges;
    }else {
      num_ranges_projector = originallaser->ranges().size();
    }
    float fov_projector;
    if (_fov){
      fov_projector = _fov;
    }else {
      fov_projector = originallaser->maxAngle() -  originallaser->minAngle();
    }

    size_t offset = 0;
    if (originallaser->ranges().size() > num_ranges_projector)
      offset = (originallaser->ranges().size() - num_ranges_projector)/2;

    projector.setFov(fov_projector);
    projector.setNumRanges(num_ranges_projector);

    FloatVector ranges;
    IntVector indices;
    projector.project(ranges, indices, transform, *c);

    //create g2o RobotLaser data from ranges
    LaserParameters lparams(0, num_ranges_projector, -fov_projector/2, originallaser->angleIncrement(), originallaser->maxRange(), 0.1, 0);
    SE2 trobotlaser(0, 0, 0); //TODO
    lparams.laserPose = trobotlaser;

    RobotLaser* rlaser = new RobotLaser;
    rlaser->setLaserParams(lparams);
    Eigen::Isometry2f estimate = v->estimate().toIsometry().cast<float>()*transform;
    rlaser->setOdomPose(estimate.cast<double>());
    rlaser->setHostname("hostname");
    rlaser->setTimestamp(originallaser->timestamp());

    //Trying to refine it using the original laser (solve problem scans traversing walls)
    std::vector<float> ranges_originallaser = originallaser->ranges();
    size_t j = 0;
    for (size_t i = offset; i < ranges_originallaser.size()-offset; i++){
      if (ranges[j] != 1000 && ranges[j] > ranges_originallaser[i] + 0.1)
	//ranges[j] = ranges_originallaser[i];
	ranges[j] = 1000;
      else if (ranges_originallaser[i] == originallaser->maxRange() || ranges[j] > originallaser->maxRange())
	ranges[j] = 1000;
      j++;
    }

    std::vector<double> rangesd(ranges.begin(), ranges.end());
    rlaser->setRanges(rangesd);

    return rlaser;
  }

  RobotLaser* SimpleGraph::computeScanFromWaypoint(VertexSE2* v, srrg_core::LaserMessage* laser, const Eigen::Isometry2f& transform){

    if (!laser)
      return 0;

    //create g2o RobotLaser data from ranges
    LaserParameters lparams(0, laser->ranges().size(), laser->minAngle(), laser->angleIncrement(), laser->maxRange(), 0.1, 0);
    // TODO: Check this (right now tracker works already on laser frame)
    // Eigen::Isometry2f laser_offset;
    // laser_offset.setIdentity();
    // Eigen::Vector2f laser_offset_t(laser->offset().translation().x(), laser->offset().translation().y());
    // laser_offset.translation() = laser_offset_t;
    // laser_offset.linear() = laser->offset().linear().block<2,2>(0,0);
    // SE2 trobotlaser(laser_offset.cast<double>());
    SE2 trobotlaser(0,0,0);
    lparams.laserPose = trobotlaser;

    RobotLaser* rlaser = new RobotLaser;
    rlaser->setLaserParams(lparams);
    Eigen::Isometry2f estimate = v->estimate().toIsometry().cast<float>()*transform;
    rlaser->setOdomPose(estimate.cast<double>());
    rlaser->setHostname("hostname");
    std::vector<double> rangesd(laser->ranges().begin(), laser->ranges().end());
    rlaser->setRanges(rangesd);
    rlaser->setTimestamp(laser->timestamp());

    return rlaser;
  }


  void SimpleGraph::checkClosures(){
    _vertices_to_merge.clear();
    LoopClosureChecker lcc;
    if (_candidate_closures.checkList(_loopClosureWindow)){
      cout << endl << "Loop Closure Checking." << endl;

      lcc.init(_candidate_closures.vertices(), _candidate_closures.edgeSet(), _loopClosureInlierThreshold);
      lcc.check();

      cout << "Best Chi2 = " << lcc.chi2() << endl;
      cout << "Inliers = " << lcc.inliers() << endl;

      if (lcc.inliers() >= _loopClosureMinInliers){
	LoopClosureChecker::EdgeDoubleMap results = lcc.closures();

	float outlierThreshold = 500;
	cout << "Results:" << endl;
	for (LoopClosureChecker::EdgeDoubleMap::iterator it= results.begin(); it!= results.end(); it++){
	  EdgeSE2* e = (EdgeSE2*) (it->first);
	  cout << "Edge from: " << e->vertices()[0]->id() << " to: " << e->vertices()[1]->id() << ". Chi2 = " << it->second <<  endl;

	  if (it->second < _loopClosureInlierThreshold){
	    cout << "Is an inlier. Adding to Graph" << endl;
	    _graph->addEdge(e);
	    OptimizableGraph::VertexSet vset;
	    vset.insert(e->vertices()[0]);
	    vset.insert(e->vertices()[1]);
	    _vertices_to_merge.insert(vset);
	  }else if (it->second > outlierThreshold){
	    cout << "Is a clear outlier. Removing from closures list" << endl;
	    _candidate_closures.removeEdge(e);
	  }

	}
      }
    }
    _candidate_closures.updateList(_loopClosureWindow);

  }

  void SimpleGraph::addEllipseData(OptimizableGraph::Vertex* v){
    VertexEllipse *ellipse = new VertexEllipse;
    Eigen::Matrix3f cov = Eigen::Matrix3f::Zero(); //Initialize EllipseData with zero covariance
    ellipse->setCovariance(cov);
    v->addUserData(ellipse);
  }

  VertexEllipse* SimpleGraph::findEllipseData(OptimizableGraph::Vertex* v){
    HyperGraph::Data* d = v->userData();
    while (d){
      VertexEllipse* ellipse = dynamic_cast<VertexEllipse*>(d);
      if (ellipse){
	return ellipse;
      }else{
	d = d->next();
      }
    }
    return 0;
  }

  void SimpleGraph::updateEllipseData(){
    if (_graph->vertices().size()<2)
      return;

    //Covariance computation
    CovarianceEstimator ce(_graph);
    OptimizableGraph::VertexSet vset;
    for (OptimizableGraph::VertexIDMap::iterator it=_graph->vertices().begin(); it!=_graph->vertices().end(); ++it) {
      OptimizableGraph::Vertex* v = (OptimizableGraph::Vertex*) (it->second);
      vset.insert(v);
    }
    ce.setVertices(vset);
    ce.setGauge(_currentVertex);
    ce.compute();
    for (OptimizableGraph::VertexIDMap::iterator it=_graph->vertices().begin(); it!=_graph->vertices().end(); ++it) {
      VertexSE2* v = dynamic_cast<VertexSE2*>(it->second);
      VertexEllipse* ellipse = findEllipseData(v);
      if (ellipse && (v != currentVertex())){
	Eigen::MatrixXd PvX = ce.getCovariance(v);
	Eigen::Matrix3d Pv = PvX;
	Eigen::Matrix3f Pvf = Pv.cast<float>();
	ellipse->setCovariance(Pvf);
	ellipse->clearMatchingVertices();
      }else {
	if(ellipse && v == currentVertex()){
	  ellipse->clearMatchingVertices();
	  for (size_t i = 0; i<ellipse->matchingVerticesIDs().size(); i++){
	    int id = ellipse->matchingVerticesIDs()[i];
	    VertexSE2* vid = dynamic_cast<VertexSE2*>(_graph->vertex(id));
	    if (vid){
	      SE2 relativetransf = _currentVertex->estimate().inverse() * vid->estimate();
	      ellipse->addMatchingVertex(relativetransf.translation().x(), relativetransf.translation().y());
	    }else //this vertex might not exist anymore
	      ellipse->addMatchingVertex(0, 0);
	  }
	}
      }
    }
  }

  RobotLaser* SimpleGraph::findLaserData(OptimizableGraph::Vertex* v){
    HyperGraph::Data* d = v->userData();

    while (d){
      RobotLaser* robotLaser = dynamic_cast<RobotLaser*>(d);
      if (robotLaser){
	return robotLaser;
      }else{
	d = d->next();
      }
    }

    return 0;
  }

  void SimpleGraph::removeVertex(VertexSE2* v){
    //if (v->id() == 0) //not safe to simply remove first vertex of the graph
    //  return;

    if (v == currentVertex()){
      int i = 1;
      int currid = currentVertex()->id();
      while ((currid-i>=0) && !graph()->vertex(currid-i)){
	i++;
      }
      VertexSE2* previousVertex = (VertexSE2*) graph()->vertex(currid-i);

      setCurrentVertex(previousVertex);
    }
    //in case we remove a fixed vertex, the current is the new fixed one
    currentVertex()->setFixed(v->fixed());

    graph()->removeVertex(v);
    Cloud2DWithTrajectory* c = _verticesClouds[v];
    _verticesClouds.erase(v);

    if (c)
      delete c;

  }


  void SimpleGraph::exportTrajectory(const char *filename){
    SparseOptimizer* graphWithTrajectory = new SparseOptimizer;

    //Transform clouds to laser scans
    for (VertexSE2Cloud2DMap::iterator it = _verticesClouds.begin(); it != _verticesClouds.end(); it++){
      VertexSE2* v = dynamic_cast<VertexSE2*>(it->first);
      Cloud2DWithTrajectory* cloud = dynamic_cast<Cloud2DWithTrajectory*>(it->second);
      for (size_t wp = 0; wp < cloud->trajectory().size(); wp++){
	Eigen::Isometry2f wp_transf = cloud->waypoint(wp);

	VertexSE2* v2 = new VertexSE2;
	Eigen::Isometry2f globalwp = v->estimate().toIsometry().cast<float>()*wp_transf;
	SE2 estimate(globalwp.cast<double>());
	v2->setEstimate(estimate);
	v2->setId(v->id()*1000+wp);
	RobotLaser* rlaser2 = computeScanFromCloudWaypoint(v, wp_transf.inverse(), wp);

	if (rlaser2)
	  v2->addUserData(rlaser2);

	graphWithTrajectory->addVertex(v2);
      }

    }

    ofstream ofs(filename);
    for (HyperGraph::VertexIDMap::iterator it=graphWithTrajectory->vertices().begin(); it!=graphWithTrajectory->vertices().end(); ++it){
      VertexSE2* vtosave= (VertexSE2*)(it->second);
      graphWithTrajectory->saveVertex(ofs, vtosave);
    }
  }

  void SimpleGraph::exportStampedTrajectory(const char *filename){
    ofstream ofs(filename);
    //Transform clouds to laser scans
    for (VertexSE2Cloud2DMap::iterator it = _verticesClouds.begin(); it != _verticesClouds.end(); it++){
      VertexSE2* v = dynamic_cast<VertexSE2*>(it->first);

      Cloud2DWithTrajectory* cloud = dynamic_cast<Cloud2DWithTrajectory*>(it->second);
      for (size_t wp = 0; wp < cloud->trajectory().size(); wp++){
	Eigen::Isometry2f wp_transf = cloud->waypoint(wp);

	srrg_core::LaserMessage* laser = cloud->laserWaypoint(wp);

	Eigen::Isometry2f globalwp = v->estimate().toIsometry().cast<float>()*wp_transf;
	SE2 estimate(globalwp.cast<double>());

	ofs << fixed << setprecision(5) << laser->timestamp() << " " << estimate.toVector().transpose() << endl;
      }

    }
  }

  void SimpleGraph::exportTrajectoryOriginalLaser(const char *filename){
    SparseOptimizer* graphWithTrajectory = new SparseOptimizer;

    //Transform clouds to laser scans
    for (VertexSE2Cloud2DMap::iterator it = _verticesClouds.begin(); it != _verticesClouds.end(); it++){
      VertexSE2* v = dynamic_cast<VertexSE2*>(it->first);

      Cloud2DWithTrajectory* cloud = dynamic_cast<Cloud2DWithTrajectory*>(it->second);
      for (size_t wp = 0; wp < cloud->trajectory().size(); wp++){
	Eigen::Isometry2f wp_transf = cloud->waypoint(wp);

	srrg_core::LaserMessage* laser = cloud->laserWaypoint(wp);

	VertexSE2* v2 = new VertexSE2;
	Eigen::Isometry2f globalwp = v->estimate().toIsometry().cast<float>()*wp_transf;
	SE2 estimate(globalwp.cast<double>());
	v2->setEstimate(estimate);
	v2->setId(v->id()*100+wp);
	RobotLaser* rlaser1 = computeScanFromWaypoint(v, laser, wp_transf);
	if (rlaser1)
	  v2->addUserData(rlaser1);
	graphWithTrajectory->addVertex(v2);
      }

    }

    ofstream ofs(filename);
    for (HyperGraph::VertexIDMap::iterator it=graphWithTrajectory->vertices().begin(); it!=graphWithTrajectory->vertices().end(); ++it){
      VertexSE2* vtosave= (VertexSE2*)(it->second);
      graphWithTrajectory->saveVertex(ofs, vtosave);
    }
  }


  void SimpleGraph::loadGraph(const char *filename){
    _graph->load(filename);

    std::string sfilename(filename);
    std::string base_filename = sfilename.substr(sfilename.find_last_of("/\\") + 1);
    std::string delimiter = ".g2o";
    std::string root = base_filename.substr(0, base_filename.find(delimiter));
    for (OptimizableGraph::VertexIDMap::iterator it=_graph->vertices().begin(); it!=_graph->vertices().end(); ++it) {
      VertexSE2* v = (VertexSE2*) (it->second);

      char cloudfilename[1024];
      sprintf(cloudfilename, "%s-cloud-%05d.dat", root.c_str(), v->id());
      ifstream cloud_stream(cloudfilename);
      Cloud2DWithTrajectory* cloud = new Cloud2DWithTrajectory;
      cloud->load(cloud_stream);
      cloud->setColor(Eigen::Vector3f(0,0.4,0));
      _verticesClouds.insert(std::make_pair(v,cloud) );

      if (v->id() >= _currentId)
	_currentId = v->id()+1;
    }
    _currentSessionId = _currentId;
  }

  void SimpleGraph::clear(){

    _currentVertex = 0;
    _currentId = 0;
    _currentSessionId = _currentId;
    _isFirstVertex = true;
    
    //clear graph
    _graph->clear();

    //clear clouds
    for (VertexSE2Cloud2DMap::iterator it = _verticesClouds.begin(); it != _verticesClouds.end(); it++){
      Cloud2DWithTrajectory* cloud = dynamic_cast<Cloud2DWithTrajectory*>(it->second);
      delete cloud;
    }
    _verticesClouds.clear();

    _vertices_to_merge.clear();
    _candidate_closures.clear();
  }  

}
