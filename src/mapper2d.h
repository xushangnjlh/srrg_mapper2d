#pragma once

//srrg includes
#include "srrg_messages/laser_message.h"

#include "laser_message_tracker.h"
#include "cloud2d_trajectory.h"
#include "simple_graph.h"
#include "graph_merger.h"

#include <chrono>

namespace srrg_mapper2d {

  struct CovarianceAccumulator {
  
    CovarianceAccumulator(){
      init();
    }
  
    void init(){
      _T.setIdentity();
      _P.setZero(); //Zero uncertainty
    }

    const Eigen::Isometry2f& T() const {return _T;} 
    const Eigen::Matrix3f& P() const {return _P;}
    void Oplus(Eigen::Isometry2f nT, Eigen::Matrix3f nP){
      Eigen::Vector3f tab=t2v(_T);
      Eigen::Vector3f tbc=t2v(nT);

      float sinab = sin(tab.z());
      float cosab = cos(tab.z());

      Eigen::Matrix3f J1Oplus;
      J1Oplus.setIdentity();
      J1Oplus(0, 2) = -tbc.x()*sinab-tbc.y()*cosab;
      J1Oplus(1, 2) = tbc.x()*cosab-tbc.y()*sinab;

      Eigen::Matrix3f J2Oplus;
      J2Oplus.setIdentity();
      J2Oplus(0, 0) = cosab;
      J2Oplus(0, 1) = -sinab;
      J2Oplus(1, 0) = sinab;
      J2Oplus(1, 1) = cosab;

      _T = _T * nT;
      _P = J1Oplus * _P * J1Oplus.transpose() + J2Oplus * nP * J2Oplus.transpose(); //tab & tbc independent
    }
  
  protected:
    Eigen::Isometry2f _T; //Transformation
    Eigen::Matrix3f   _P; //Covariance
  };


  struct PoseAccumulator {
    //Keeps track of current pose of the robot
    //New estimates arrive when new vertices are added into the graph
    //Meanwhile, tracker can also provide increments between vertices
    //which are applied to previous estimates to obtain a new rough estimate
    //Tracker can skip scans for efficiency so intermediate odom measurements are also considered
    PoseAccumulator() {
      init();
    }
    void init() {
      _estimate = Eigen::Isometry2f::Identity();
      _tracker = Eigen::Isometry2f::Identity();
      _odom = Eigen::Isometry2f::Identity();
      _relative_odom = Eigen::Isometry2f::Identity();
    }
    
    Eigen::Isometry2f estimate() const {
      Eigen::Isometry2f est = _estimate*_relative_odom;
      return est;
    }
    // Sets last estimate, we also need current tracker and odom pose for future computation of increments
    void setEstimate(const Eigen::Isometry2f& estimate, const Eigen::Isometry2f& tracker, const Eigen::Isometry2f& odom) {
      _estimate = estimate;
      _tracker = tracker;
      _odom = odom;
      _relative_odom = Eigen::Isometry2f::Identity();
    }
  
    void setTracker(const Eigen::Isometry2f& tracker, const Eigen::Isometry2f& odom) {
      //Compute relative tracker pose since last pose update
      Eigen::Isometry2f relative_tracker = _tracker.inverse()*tracker;
      //Update estimate
      _estimate = _estimate * relative_tracker;
    
      _tracker = tracker;
      _odom = odom;
      _relative_odom = Eigen::Isometry2f::Identity();
    }

    void setOdom(const Eigen::Isometry2f& odom){
      //Accumulate odometry since last tracker measurement
      _relative_odom = _relative_odom * _odom.inverse() * odom;
      _odom = odom;
    }
  
  protected:
    Eigen::Isometry2f _estimate;
    Eigen::Isometry2f _tracker;
    Eigen::Isometry2f _odom;
    Eigen::Isometry2f _relative_odom;
  };

  class Mapper2D {
  public:

    Mapper2D(LaserMessageTracker* tracker, SimpleGraph* graph_map);

    inline void setVertexTranslationThreshold(double vertex_translation_threshold) {_vertex_translation_threshold = vertex_translation_threshold;}
    inline void setVertexRotationThreshold(double vertex_rotation_threshold) {_vertex_rotation_threshold = vertex_rotation_threshold;}

    inline void setUseMerger(bool use_merger) {_use_merger = use_merger;}
    inline void setVerbose(bool verbose) {_verbose = verbose;}
    void setLogTimes(bool log_times);
    inline void setMultiSession(bool multi_session) {_multi_session = multi_session;}
    inline bool multiSessionStarted() {return _multi_session_started;}
    inline bool forceDisplay() {return _force_display;}
    inline GraphMerger graphMerger(){return _gmerger;}
    inline LaserMessageTracker* tracker() {return _tracker;}
    inline SimpleGraph* graphMap() {return _graph_map;}
    inline Eigen::Isometry2f estimate() {return _pa.estimate();}

    inline Cloud2DWithTrajectory* currentCloudTrajectory() {return _current_cloud_trajectory;}

    void init(LaserMessage* las, const Eigen::Isometry2f& init_pose = Eigen::Isometry2f::Identity());
    void initMultiSession(const Eigen::Isometry2f& start_session_pose = Eigen::Isometry2f::Identity());
    bool compute(LaserMessage* las);
    void finishMapping();
    void saveData();
    void clear();
      
  protected:

    LaserMessageTracker* _tracker;
    SimpleGraph* _graph_map;

    Eigen::Isometry2f _previous_vertex_tracker_t;
    Eigen::Isometry2f _previous_tracker_t;

    CovarianceAccumulator _ca;
    PoseAccumulator _pa;
    
    bool _track_new_cloud;
    bool _first_vertex;
    bool _use_merger;
    bool _verbose;
    bool _multi_session;
    bool _multi_session_started;
    bool _force_display;
    bool _log_times;
    std::ofstream* _log_stream;

    Cloud2DWithTrajectory* _current_cloud_trajectory;
    GraphMerger _gmerger;
    Cloud2DWithTrajectoryMerger _cmerger;

    double _vertex_translation_threshold;
    double _vertex_rotation_threshold;

  };

}
